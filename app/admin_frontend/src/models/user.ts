import Model, {HttpMap, Num, Pk} from "./model";

@HttpMap({
    url: {detail: '/auth/user/'},
    key: {detail: null}
})
export default class User extends Model<User> {
    @Num() @Pk pk: number;
    last_name: string;
    first_name: string;
    username: string;
    email: string;

    password1: string;
    password2: string;
}