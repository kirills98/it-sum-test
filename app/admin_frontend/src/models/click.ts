import Model, {Arr, DateTime, HttpMap, Num, Pk} from "./model";
import {Moment} from "moment";

@HttpMap({
    url: {list: '/click/?site={site}'},
    key: {list: null}
})
export default class Click extends Model<Click> {
    @Num() @Pk pk: number;
    @Num() x: number;
    @Num() y: number;
    path: string;
    @Num() user_tz: number;
    @DateTime() created_at: Moment;
}