import 'reflect-metadata';
import * as moment from 'moment';
import {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";
import {FORMAT_DATETIME_BACK} from "../consts/formats";

type ModelUrl = 'list' | 'detail' | 'remove';
type UrlParams = { pk?: any } | any;
type Fields<T> = (keyof T | { name: keyof T, fields: Fields<any> })[];

interface IHttpMap {
    url?: {
        list?: string,
        detail?: string,
        remove?: string
    },
    key?: {
        list?: string,
        detail?: string
    }
}

interface IResponse<T> {
    data: T,
    response: AxiosResponse
}

interface IHttpParams {
    url?: UrlParams,
    axios?: AxiosRequestConfig
}

export default class Model<T extends Model<T>> {
    constructor(data: Partial<T>) {
        Object.assign(this, data);
        data = Object.assign({}, {...(data as any)});
        let transform = this.meta('transform') || {};
        Object.keys(transform).forEach(prop => {
            let params = transform[prop];
            params.forEach(t => {
                if ([undefined, null].includes(data[prop]) || !!t.type && typeof data[prop] !== t.type) return this[prop] = data[prop];
                data[prop] = this[prop] = t.to(data[prop]);
            })
        })
    }

    /**
     * Prepare model for XHR request
     */
    toPost(fields: Fields<T> = null): Partial<T> {
        let result = {};
        ((fields as any) || Object.keys(this)).forEach(f => {
            let field = typeof f === 'object' ? f.name : f;
            let fs = typeof f === 'object' ? f.fields : undefined;
            let value = this[field];
            if (value === undefined) return;
            let serializeObject = () => {
                if (typeof value !== 'object') return;
                let toPost = v => typeof v === 'object' ? (v.toPost ? v.toPost(fs) : this.toPost.apply(value, [fs])) : v;
                if (value['map']) result[field] = value.map(toPost);
                else result[field] = toPost(value);
                value = result[field];
            };
            if (typeof f === 'string') {
                if (value instanceof Model || value instanceof Array) {
                    if (!(fields || []).includes(field)) return;
                    serializeObject();
                }
                if (this.meta) {
                    let transform = this.meta('transform')[f];
                    if (transform) return transform.reverse().forEach(t => result[f] = t.from(value))
                }
                result[f] = value;
            }
            else if (typeof f === 'object') {
                return serializeObject()
            }
        });
        return result;
    }

    /**
     * Wrap array of object to model class
     */
    static wrap<T extends Model<T>>(data: Partial<T>[]): T[] {
        return data.map(o => new this(o)) as any[];
    }

    /**
     * Get list of wrapped objects by XHR request
     */
    static async list<T extends Model<T>>(http: AxiosInstance, params: IHttpParams = {}): Promise<IResponse<T[]>> {
        let response = await http.get(this.httpUrl('list', params.url), params.axios);
        return {
            data: this.wrap(this.httpKey.list ? response.data[this.httpKey.list] : response.data) as T[],
            response
        };
    }

    /**
     * Get some wrapped object by XHR request
     */
    static async detail<T extends Model<T>>(http: AxiosInstance, params: IHttpParams = {}): Promise<IResponse<T>> {
        let response = await http.get(this.httpUrl('detail', params.url), params.axios);
        return {
            data: new this(this.httpKey.detail ? response.data[this.httpKey.detail] : response.data) as T,
            response
        };
    }

    /**
     * Remove some object by XHR request
     */
    async remove<T extends Model<T>>(http: AxiosInstance, params: IHttpParams = {}): Promise<AxiosResponse> {
        return await http.get(this.httpUrl('remove', params.url), params.axios)
    }

    /**
     * Get keys from HttpMap
     */
    static get httpKey(): any {
        return ((this.meta('httpMap') || {}) as IHttpMap).key || {};
    }

    /**
     * Get urls from HttpMap with params interpolation (auto set PK)
     */
    httpUrl(type: ModelUrl, params: UrlParams = {}): string {
        if (!params.pk) params.pk = this.Pk;
        return (this.constructor as any).httpUrl(type, params)
    }

    /**
     * Get urls from HttpMap with params interpolation
     */
    static httpUrl(type: ModelUrl, params: UrlParams = {}): string {
        let map = this.meta('httpMap') as IHttpMap;
        if (!map || !map.url || !map.url[type]) return null;
        let url = map.url[type];
        if (!url) throw new Error(`Url for '${type}' not defined.`);
        Object.keys(params).forEach(k => url = url.replace(`{${k}}`, params[k]));
        let unusedParams = url.match(/{([^\/{}]+)}/img);
        if (unusedParams && unusedParams.length)
            throw new Error(`Params ${unusedParams.join(', ')} is not defined.`);
        return url
    }

    /**
     * Get primary key by metadata
     */
    get Pk() {
        return this[this.meta('pk')];
    }

    /**
     * Get some value from class metadata
     */
    meta(key: string): any {
        return (this.constructor as any).meta(key)
    }

    /**
     * Get some value from class metadata
     */
    static meta(key: string): any {
        return Reflect.getMetadata(key, this)
    }
}

/**
 * Set to model class params for http requests
 */
export function HttpMap(params: IHttpMap) {
    return (target: any) => {
        Reflect.defineMetadata('httpMap', params, target);
    }
}

/**
 * Define property as primary key
 */
export function Pk(target: any, key: string) {
    let Type = target.constructor;
    Reflect.defineMetadata('pk', key, Type);
}

/**
 * Convert by callback
 */
export function Transform(to: Function, from: Function, type: string = 'string') {
    return (target: any, key: string) => {
        let Type = target.constructor;
        let transforms = Type.meta('transform') || {};
        if (!transforms[key]) transforms[key] = [];
        transforms[key].unshift({to, from, type});
        Reflect.defineMetadata('transform', transforms, Type);
    }
}

/**
 * Convert to numeric value
 */
export function Num(type: string = null) {
    return Transform(Number, v => v, type)
}

/**
 * Convert to boolean value
 */
export function Bool(type: string = null) {
    return Transform(v => !!+v, v => +v, type)
}

/**
 * Convert from JSON value
 */
export function Json(type: string = null) {
    return Transform(JSON.parse, JSON.stringify, type)
}

/**
 * Convert to Moment object by string datetime
 */
export function DateTime(format: string = FORMAT_DATETIME_BACK, type: string = null) {
    return Transform(v => moment(v, format), v => v.format(format), type)
}

/**
 * Convert to Moment object by timestamp
 */
export function Unix(type: string = null) {
    return Transform(moment.unix, v => v.unix(), type)
}

/**
 * Convert to array by callback
 */
export function Arr(to: Function = v => v, from: Function = v => v, type: string = null) {
    return Transform(v => v.map(to), v => v.map(from), type)
}

/**
 * Convert to array of class instances
 */
export function ArrayOf(cls: { new(...args) }, type: string = null) {
    return Transform(v => v.map(o => new cls(o)), v => v, type)
}

/**
 * Convert to class instance
 */
export function Class(cls: { new(...args) }, type: string = null) {
    return Transform(v => new cls(v), v => v, type)
}