import Model, {Arr, HttpMap, Num, Pk} from "./model";

@HttpMap({
    url: {list: '/site/', detail: '/site/{pk}/'},
    key: {list: null, detail: null}
})
export default class Site extends Model<Site> {
    @Num() @Pk pk: number;
    name: string;
    @Arr() domains: string[];
}