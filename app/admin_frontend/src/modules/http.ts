import axios, {AxiosResponse} from 'axios'
import {Stores} from "../stores";
import {STORE_USER} from "../consts/stores";

function renderError(errors: object): string {
    if (!errors) return '';
    if (['number', 'string', 'symbol'].includes(typeof errors)) return errors as any;

    if (typeof errors === 'object') {
        let message = '';
        Object.keys(errors).forEach(k => {
            message += `${isNaN(k as any) ? `${k}: ` : ''} ${renderError(errors[k])}\n`
        });
        return message;
    }
    return '';

}

let http = (stores: Stores) => {
    let http = axios.create({
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        baseURL: '/api/'
    });
    http.interceptors.response.use(
        res => res,
        err => {
            let res: AxiosResponse = err.response;
            if (res) {
                if (res.status == 401) {
                    stores[STORE_USER].logout(false);
                } else if (res.data && typeof res.data === 'object') {
                    console.log(res.data)
                    let message = renderError(res.data)
                    alert(message);
                    console.log(message)
                } else {
                    console.log(res);
                    alert(`${res.status}: ${res.statusText}`)
                }
            }
            else {
                console.log(err);
                alert("Ошибка");
            }
            return Promise.reject(err)
        }
    );
    return http;
};

export default http