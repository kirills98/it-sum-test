import {withRouter as wr} from "react-router";

export function withRouter(component) {
    return wr(component) as any;
}