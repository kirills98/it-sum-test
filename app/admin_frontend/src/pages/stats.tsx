import * as React from "react";
import Site from "../models/site";
import {inject} from "mobx-react";
import {STORE_BUS, STORE_HTTP} from "../consts/stores";
import {Stores} from "../stores";
import {Link, RouteComponentProps, Switch, Redirect, Route, NavLink} from "react-router-dom";
import Click from "../models/click";
import * as moment from "moment";
import {Moment} from "moment";
import {FORMAT_INPUT} from "../consts/formats";
import {HeatMap} from "../components/heatmap";
import {Async as SelectAsync} from 'react-select'
import {Chart} from "../components/chart";
import {withRouter} from "../modules/observer-with-router";
import {EVENT_HEATMAP_CLICKS, EVENT_HEATMAP_OVERLAY} from "../consts/events";

interface Props extends Partial<Stores>, RouteComponentProps<{ pk: string }> {
}

interface State {
    site: Site;
    clicks: any[];
    filter: {
        start: Moment;
        end: Moment;
        path: string;
    }
    _path: string;
    overlay: boolean;
}

@inject(STORE_HTTP, STORE_BUS)
@withRouter
export class Stats extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            site: null,
            clicks: [],
            filter: {
                end: moment(),
                start: (moment() as Moment).add(-1, 'days'),
                path: '/'
            },
            _path: '/',
            overlay: true
        };
    }

    componentDidMount() {
        this.getSite();
        this.getClicks();
        this.props[STORE_BUS].on(EVENT_HEATMAP_OVERLAY, overlay => this.setState({overlay}));
    }

    async getSite() {
        let pk = this.props.match.params.pk;
        let r = await Site.detail<Site>(this.props[STORE_HTTP], {url: {pk}});
        this.setState({site: r.data})
    }

    async searchClicks(input) {
        let site = this.props.match.params.pk;
        console.log('asdf');
        let r = await this.props[STORE_HTTP].get('/click/search/', {
            params: {path__contains: input, site}
        });

        return {options: r.data.map(c => ({label: c.path, value: c.path})), complete: true};
    }

    async getClicks() {
        let site = this.props.match.params.pk;
        this.handleInput('path', this.state._path);
        let r = await Click.list<Click>(this.props[STORE_HTTP], {
            url: {site},
            axios: {
                params: {
                    created_at__lte: this.state.filter.end.toISOString(true),
                    created_at__gte: this.state.filter.start.toISOString(true),
                    // path__istartswith: this.state.filter.path
                    path: this.state.filter.path
                }
            }
        });
        this.setState({clicks: r.data});
        this.props[STORE_BUS].emit(EVENT_HEATMAP_CLICKS);
    }

    handleInput(field: keyof { start, end, path }, val) {
        let m = null;
        if (field !== 'path') {
            m = moment(val, FORMAT_INPUT);
        } else {
            m = val
        }
        let f = this.state.filter;
        f[field] = m;
        this.setState({filter: f})
    }

    get url() {
        if (!this.state.site) return '';
        return `http://${this.state.site.domains[0]}${this.state.filter.path}`
    }

    render() {
        return (
            <div className="mt-3 mr-3 ml-3 mb-3">
                <div className="row">
                    <div className="col-sm-6 form-group">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">От</span>
                            </div>
                            <input type="datetime-local"
                                   className="form-control"
                                   value={this.state.filter.start.format(FORMAT_INPUT)}
                                   onChange={e => this.handleInput("start", e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="col-sm-6 form-group">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">До</span>
                            </div>
                            <input type="datetime-local"
                                   className="form-control"
                                   value={this.state.filter.end.format(FORMAT_INPUT)}
                                   onChange={e => this.handleInput("end", e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="col-sm-auto form-group pr-0">
                        {this.state.site &&
                        <span>http://{this.state.site.domains[0]}</span>
                        }
                    </div>
                    <div className="col-sm form-group">
                        <SelectAsync loadOptions={this.searchClicks.bind(this)}
                                     name="test"
                                     value={this.state._path}
                                     placeholder={"test1"}
                                     loadingPlaceholder={"test2"}
                                     addLabelText={"test4"}
                                     onChange={opt => this.setState({_path: opt ? (opt as any).value : '/'})}
                        />
                    </div>
                    <div className="col-sm-4 form-group">
                        <button className="btn btn-primary mr-1" onClick={() => this.getClicks()}>Найти</button>
                        <button className={`btn btn-${this.state.overlay ? 'default' : 'secondary'}`}
                                onClick={() => this.props[STORE_BUS].emit(EVENT_HEATMAP_OVERLAY, !this.state.overlay)}>
                            {this.state.overlay ? 'Скрыть' : 'Показать'} overlay
                        </button>
                    </div>
                </div>
                <div className="mb-2">
                    <ul className="nav nav-tabs">
                        <li className="nav-item">
                            <NavLink activeClassName="active"
                                     to={`${this.props.match.url}/heatmap`}
                                     className="nav-link"
                            >Карта кликов</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink activeClassName="active"
                                     to={`${this.props.match.url}/chart`}
                                     className="nav-link"
                            >Статистика</NavLink>
                        </li>
                    </ul>
                </div>
                <div>
                    <Route exact
                           path={`${this.props.match.url}/heatmap`}
                           render={() => this.state.site &&
                               <HeatMap clicks={this.state.clicks} path={this.url}/>
                           }
                    />
                    <Route
                        exact
                        path={`${this.props.match.url}/chart`}
                        render={() => <Chart clicks={this.state.clicks}/>}
                    />
                </div>
            </div>
        )
    }
}
