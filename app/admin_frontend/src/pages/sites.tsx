import * as React from "react";
import Site from "../models/site";
import {inject} from "mobx-react";
import {STORE_HTTP} from "../consts/stores";
import {Stores} from "../stores";
import {Link} from "react-router-dom";

interface State {
    sites: Site[];
}

@inject(STORE_HTTP)
export class Sites extends React.Component<Partial<Stores>, State> {
    constructor(props) {
        super(props);
        this.state = {sites: []};
    }

    componentDidMount() {
        this.getSites()
    }

    async getSites() {
        let r = await Site.list<Site>(this.props[STORE_HTTP]);
        this.setState({sites: r.data})
    }

    async removeSite(site: Site) {
        if (!confirm("Подтвердите действие")) return;
        await this.props[STORE_HTTP].delete(`/site/${site.Pk}/`);
        this.getSites();
    }

    render() {
        return (
            <div className="mt-3 mr-3 ml-3">
                <div className="mb-1">
                    <Link to="/sites/form/create" className="btn btn-primary btn-sm">Добавить</Link>
                </div>
                <table className="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Домены</th>
                        <th scope="col"/>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.sites.map((s, i) => (
                        <tr key={s.Pk}>
                            <th scope="row">{i + 1}</th>
                            <td>{s.name}</td>
                            <td>{s.domains.join(', ')}</td>
                            <td>
                                <Link to={`/sites/stats/${s.Pk}/heatmap`}
                                      className="btn btn-sm btn-primary mr-1">
                                    <i className="fa fa-bar-chart"/>
                                </Link>
                                <Link to={`/sites/form/${s.Pk}`}
                                      className="btn btn-sm btn-warning mr-1">
                                    <i className="fa fa-edit"/>
                                </Link>
                                <button className="btn btn-sm btn-danger"
                                        onClick={() => this.removeSite(s)}
                                ><i className="fa fa-remove"/></button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        )
    }
}
