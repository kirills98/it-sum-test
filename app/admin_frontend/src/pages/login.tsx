import * as React from "react";
import {inject} from "mobx-react";
import {STORE_USER} from "../consts/stores";
import {Stores} from "../stores";

interface State {
    user: User
}

interface User {
    username: string;
    password: string;
    remember: boolean;
}

@inject(STORE_USER)
export class Login extends React.Component<Partial<Stores>, State> {

    constructor(props, context) {
        super(props, context);
        this.state = {user: {password: '', username: '', remember: false}}
    }

    handleSubmit(e: Event) {
        e.preventDefault();
        this.props[STORE_USER].login(
            this.state.user.username,
            this.state.user.password,
            this.state.user.remember
        );
    }

    handleInput(field: keyof User, val) {
        this.state.user[field] = val;
        this.setState(this.state);
    }

    render() {
        return (
            <div className="row justify-content-center mt-3 mr-3 ml-3">
                <div className="card col-xs-12 col-sm-8">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="form-group">
                                <label htmlFor="username">Username</label>
                                <input className="form-control"
                                       id="username"
                                       placeholder="Enter username"
                                       value={this.state.user.username}
                                       onInput={e => this.handleInput('username', e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input className="form-control"
                                       id="password"
                                       placeholder="Enter password"
                                       type="password"
                                       value={this.state.user.password}
                                       onInput={e => this.handleInput('password', e.target.value)}
                                />
                            </div>
                            <div className="form-check">
                                <label className="form-check-label">
                                    <input type="checkbox"
                                           className="form-check-input"
                                           onChange={e => this.handleInput('remember', e.target.checked)}
                                    />
                                    Remember
                                </label>
                            </div>
                            <button type="submit" className="btn btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
