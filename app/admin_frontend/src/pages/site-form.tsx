import * as React from "react";
import {inject} from "mobx-react";
import {STORE_HTTP, STORE_USER} from "../consts/stores";
import {Stores} from "../stores";
import Site from "../models/site";
import {RouteComponentProps} from "react-router";

interface Props extends RouteComponentProps<{ param: string }>, Partial<Stores> {
}

interface State {
    site: Partial<Site>
}

@inject(STORE_HTTP)
export class SiteForm extends React.Component<Props, State> {

    constructor(props, context) {
        super(props, context);
        this.state = {site: new Site({domains: [''], name: '', pk: undefined})}
    }

    async handleSubmit(e: Event) {
        e.preventDefault();
        let data = this.state.site.toPost(['name', 'domains', 'pk']);
        if (this.props.match.params.param === 'create')
            await this.props[STORE_HTTP].post('/site/', data);
        else
            await this.props[STORE_HTTP].put(`/site/${data.pk}/`, data);
        this.props.history.push('/sites');
    }

    componentDidMount() {
        console.log(this.props);
        if (this.props.match.params.param !== 'create')
            this.getSite();
    }

    async getSite() {
        let pk = this.props.match.params.param;
        let r = await Site.detail<Site>(this.props[STORE_HTTP], {url: {pk}});
        this.setState({site: r.data})
    }

    handleInput(field: keyof Site, val) {
        this.state.site[field as any] = val;
        this.setState(this.state);
    }

    render() {
        return (
            <div className="row justify-content-center mt-3 mr-3 ml-3">
                <div className="card col-xs-12 col-sm-8">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="form-group">
                                <label htmlFor="name">Название</label>
                                <input className="form-control"
                                       id="name"
                                       placeholder="Enter name"
                                       value={this.state.site.name}
                                       onChange={e => this.handleInput('name', e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <div>
                                    <label>Домены</label>
                                    &nbsp;
                                    <button className="btn btn-outline-success btn-sm"
                                            type="button"
                                            onClick={() => {
                                                this.state.site.domains.push('');
                                                this.handleInput('domains', this.state.site.domains);
                                            }}
                                    >
                                        <i className="fa fa-plus"/>
                                    </button>
                                </div>
                                {this.state.site.domains.map((d, i, arr) => (
                                    <div className="input-group mb-3" key={i}>
                                        <input className="form-control"
                                               placeholder="Enter domain"
                                               value={d}
                                               onChange={e => {
                                                   arr[i] = e.target.value;
                                                   this.handleInput('domains', arr);
                                               }}
                                        />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-danger"
                                                    type="button"
                                                    onClick={() => {
                                                        arr.splice(i, 1);
                                                        this.handleInput('domains', arr)
                                                    }}
                                            >
                                                <i className="fa fa-remove"/>
                                            </button>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            <button type="submit" className="btn btn-primary">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
