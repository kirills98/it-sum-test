import * as React from "react";
import {inject} from "mobx-react";
import {STORE_USER} from "../consts/stores";
import {Stores} from "../stores";
import User from "../models/user";
import {Input} from "../components/input";

interface State {
    user: User
}

@inject(STORE_USER)
export class Registration extends React.Component<Partial<Stores>, State> {

    constructor(props, context) {
        super(props, context);
        this.state = {user: new User({username: '', email: '', password1: '', password2: ''})}
    }

    handleSubmit(e: Event) {
        e.preventDefault();
        this.props[STORE_USER].registration(this.state.user);
    }

    handleInput(field: keyof User, val) {
        this.state.user[field as any] = val;
        this.setState(this.state);
    }

    render() {
        return (
            <div className="row justify-content-center mt-3 mr-3 ml-3">
                <div className="card col-xs-12 col-sm-8">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <Input
                                value={this.state.user.username}
                                onChange={e => this.handleInput('username', e.target.value)}
                                label="User name *"
                                name="username"
                                placeholder="Enter user name"
                            />
                            <Input
                                value={this.state.user.password1}
                                onChange={e => this.handleInput('password1', e.target.value)}
                                label="Password *"
                                name="password1"
                                placeholder="Enter password"
                                type="password"
                            />
                            <Input
                                value={this.state.user.password2}
                                onChange={e => this.handleInput('password2', e.target.value)}
                                label="Confirm password *"
                                name="password2"
                                placeholder="Enter password confirmation"
                                type="password"
                            />
                            <Input
                                value={this.state.user.email}
                                onChange={e => this.handleInput('email', e.target.value)}
                                label="Email"
                                name="email"
                                placeholder="Enter email"
                            />
                            <button type="submit" className="btn btn-primary">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
