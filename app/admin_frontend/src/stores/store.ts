import {Stores} from "./index";

export default class Store {
    protected stores: Stores = null;

    constructor(stores: Stores) {
        this.stores = stores;
    }
}