import {action, computed, observable} from "mobx";
import Store from "./store";
import {STORE_HTTP} from "../consts/stores";
import User from "../models/user";
import * as Cookie from 'js-cookie';
import {Stores} from "./index";

export const TOKEN_COOKIE = 'auth_token';

interface ILoginResponse {
    key: string
}

export default class UserStore extends Store {
    constructor(stores: Stores) {
        super(stores);
        let token = Cookie.get(TOKEN_COOKIE);
        if (token) {
            this.setToken(token);
            this.getUser();
        }
    }

    @observable user: any = null;
    @observable private _token: string = null;

    @computed
    get isAuth() {
        return !!this.token
    }

    @action
    async logout(physicalLogout = true) {
        if (physicalLogout)
            await this.stores[STORE_HTTP].post('/auth/logout/');
        this.setToken(null);
    }

    async getUser() {
        try {
            this.user = (await User.detail(this.stores[STORE_HTTP])).data;
        } catch (e) {
            this.setToken(null);
            throw e;
        }
    }

    @action
    async login(username: string, password: string, remember: boolean = false) {
        let r = await this.stores[STORE_HTTP].post<ILoginResponse>('/auth/login/', {username, password});
        this.setToken(r.data.key, remember);
        this.getUser();
    }

    @action
    async registration(user: Partial<User>) {
        let r = await this.stores[STORE_HTTP].post<ILoginResponse>('/auth/registration/',
            user.toPost([
                "username",
                'password1',
                'password2',
                'email'
            ])
        );
        this.setToken(r.data.key, true);
        this.getUser();
    }

    @action
    private setToken(val = null, remember: boolean = false) {
        this._token = val;
        if (val) {
            this.stores[STORE_HTTP].defaults.headers['Authorization'] = `Token ${val}`;
            if (remember) Cookie.set(TOKEN_COOKIE, this.token);
        }
        else {
            delete this.stores[STORE_HTTP].defaults.headers['Authorization'];
            Cookie.remove(TOKEN_COOKIE);
        }
    }

    @computed
    get token() {
        return this._token;
    }
}