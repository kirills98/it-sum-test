import {STORE_BUS, STORE_HTTP, STORE_USER} from "../consts/stores";
import UserStore from "./user-store";
import http from "../modules/http";
import {AxiosInstance} from "axios";
import Mitt from 'mitt';

export interface Stores {
    [STORE_USER]: UserStore,
    [STORE_HTTP]: AxiosInstance,
    [STORE_BUS]: Mitt.Emitter
}

const stores: Stores = {} as any;
stores[STORE_HTTP] = http(stores);
stores[STORE_USER] = new UserStore(stores);
stores[STORE_BUS] = new Mitt();

export default stores;