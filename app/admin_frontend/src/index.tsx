import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'react-select/dist/react-select.css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {BrowserRouter, Router} from 'react-router-dom';
import {Provider} from 'mobx-react';
import stores from "./stores";
import {App} from "./components/app";
// import createBrowserHistory from 'history/createBrowserHistory';
// import {RouterStore, syncHistoryWithStore} from 'mobx-react-router';

// const browserHistory = createBrowserHistory();
// const routingStore = new RouterStore();
// const history = syncHistoryWithStore(browserHistory, routingStore);

ReactDOM.render(
    <Provider {...stores} >
        {/*<Router history={history}>*/}
        <BrowserRouter>
            <App/>
        </BrowserRouter>
        {/*</Router>*/}
    </Provider>,
    document.getElementById('react-app')
);