import * as React from "react";
import Click from "../models/click";
import {inject} from "mobx-react";
import {STORE_BUS, STORE_HTTP} from "../consts/stores";
import {Stores} from "../stores";
import {EVENT_HEATMAP_CLICKS, EVENT_HEATMAP_OVERLAY} from "../consts/events";

interface Props extends Partial<Stores> {
    clicks: Click[];
    path: string;
}

@inject(STORE_HTTP, STORE_BUS)
export class HeatMap extends React.Component<Props> {

    static defaultProps: Partial<Props> = {
        clicks: [],
        path: ''
    };
    map = null;
    frame = React.createRef<HTMLIFrameElement>();
    frameLoaded = false;

    constructor(props) {
        super(props);
        this.handleFrameLoad = this.handleFrameLoad.bind(this);
        this.handleOverlay = this.handleOverlay.bind(this);
    }

    componentDidMount() {
        this.props[STORE_BUS].on(EVENT_HEATMAP_CLICKS, this.handleFrameLoad);
        this.props[STORE_BUS].on(EVENT_HEATMAP_OVERLAY, this.handleOverlay);
    }

    handleFrameLoad() {
        this.props[STORE_BUS].emit(EVENT_HEATMAP_OVERLAY, true);
        setTimeout(() => this.message('clicks', {clicks: this.props.clicks}))
    }

    handleOverlay(status) {
        this.message('overlay', {status})
    }

    componentWillUnmount() {
        this.props[STORE_BUS].off(EVENT_HEATMAP_CLICKS, this.handleFrameLoad);
        this.props[STORE_BUS].off(EVENT_HEATMAP_OVERLAY, this.handleOverlay);
    }

    message(message: string, data: any = {}) {
        if (!this.frameLoaded) return;
        this.frame.current.contentWindow.postMessage(
            JSON.stringify({message, ...data}),
            this.props.path
        );
    }

    render() {
        return (
            <div className="w-100 card" style={{height: '80vh'}}>
                <iframe src={this.props.path}
                        style={{width: '100%', height: '100%', border: 'none'}}
                        ref={this.frame}
                        onLoad={() => {
                            this.props[STORE_BUS].emit(EVENT_HEATMAP_CLICKS);
                            this.frameLoaded = true;
                        }}
                        onLoadStart={() => this.frameLoaded = false}
                />
            </div>
        )
    }
}
