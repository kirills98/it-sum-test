import * as React from "react";
import {Link, Route} from "react-router-dom";
import {inject, observer} from "mobx-react";
import {STORE_USER} from "../consts/stores";
import {Stores} from "../stores";
import {withRouter} from "../modules/observer-with-router";

const BsNavLink = ({to, children}) => (
    <Route path={to} exact={true} children={({match}) => (
        <li className={`nav-item ${match ? 'active' : ''}`}>
            <Link to={to} className="nav-link">{children}</Link>
        </li>
    )}/>
);

interface Props extends Partial<Stores> {
}

@withRouter
@inject(STORE_USER)
@observer
export class Header extends React.Component<Props> {
    render() {
        return (
            <header>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link to='/' className="navbar-brand">
                        {
                            this.props[STORE_USER].isAuth && this.props[STORE_USER].user ?
                                (<span> <i className="fa fa-user"/> {this.props[STORE_USER].user.username}</span>) :
                                'IT Sum Test'
                        }
                    </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            {this.props[STORE_USER].isAuth && <BsNavLink to="/sites">Сайты</BsNavLink>}
                        </ul>
                        {this.props[STORE_USER].isAuth ? (
                            <ul className="navbar-nav">
                                <BsNavLink to="/logout">Выход</BsNavLink>
                            </ul>
                        ) : (
                            <ul className="navbar-nav">
                                <BsNavLink to="/login">Войти</BsNavLink>
                                <BsNavLink to="/registration">Регистрация</BsNavLink>
                            </ul>
                        )}
                    </div>
                </nav>
            </header>
        )
    }
}
