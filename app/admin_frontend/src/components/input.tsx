import * as React from "react";
import {FormEvent} from "react";

interface Props {
    value: string | number;
    onChange: (e: FormEvent<HTMLInputElement>) => void;
    name?: string;
    placeholder?: string;
    label?: string;
    type?: string;
}

export class Input extends React.Component<Props> {
    static defaultProps: Partial<Props> = {
        type: 'text'
    };

    render() {
        return (
            <div className="form-group">
                {this.props.label &&
                <label htmlFor={this.props.name}>{this.props.label}</label>
                }
                <input className="form-control"
                       id={this.props.name}
                       name={this.props.name}
                       type={this.props.type}
                       placeholder={this.props.placeholder}
                       value={this.props.value}
                       onInput={this.props.onChange}
                />
            </div>
        )
    }
}
