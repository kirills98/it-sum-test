import * as React from "react";
import {Stores} from "../stores";
import {Redirect, Route, RouteProps} from "react-router";
import {inject, observer} from "mobx-react";
import {STORE_USER} from "../consts/stores";

interface RProps extends RouteProps, Partial<Stores> {
    auth?: boolean;
}

@inject(STORE_USER)
@observer
export default class AuthRoute extends React.Component<RProps> {
    static defaultProps: Partial<RProps> = {auth: true};

    render() {
        return (
            this.props[STORE_USER].isAuth === this.props.auth ?
                <Route {...this.props}/> :
                <Redirect {...this.props} to={this.props.auth ? "/login" : '/'}/>
        )
    }
}