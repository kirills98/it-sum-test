import * as React from "react";
import {Redirect, Switch, Route} from "react-router";
import AuthRoute from "./auth-route";
import {Login} from "../pages/login";
import {STORE_USER} from "../consts/stores";
import {inject} from "mobx-react";
import {Sites} from "../pages/sites";
import {SiteForm} from "../pages/site-form";
import {Stats} from "../pages/stats";
import {Registration} from "../pages/registration";

@inject(STORE_USER)
export class Main extends React.Component {
    render() {
        return (
            <main className="container-fluid">
                <Switch>
                    <Redirect path="/" to="/sites" exact/>
                    <AuthRoute path="/sites/" component={Sites} exact/>
                    <AuthRoute path="/sites/form/:param" component={SiteForm} exact/>
                    <AuthRoute path="/sites/stats/:pk" component={Stats}/>
                    <AuthRoute path="/login/" auth={false} component={Login} exact/>
                    <AuthRoute path="/registration/" auth={false} component={Registration} exact/>
                    <AuthRoute path="/logout/"
                               render={() => this.props[STORE_USER].logout() && <Redirect to={'/login/'}/>}
                               exact
                    />
                    <Redirect path="*" to="/" exact/>
                </Switch>
            </main>
        )
    }
}


