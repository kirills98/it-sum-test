import * as React from "react";
import * as moment from 'moment';
import Click from "../models/click";
import {Bar} from 'react-chartjs-2';

interface Props {
    clicks: Click[]
}

export class Chart extends React.Component<Props> {

    get data() {
        let hours = new Array(24).fill(0).map((v, i) => moment(String(i), 'HH'));
        let data = hours.map(h => {
            return this.props.clicks.filter(
                c => c.created_at.clone().utcOffset(c.user_tz).hours() === h.hours()
            ).length / this.props.clicks.length * 100;
        });
        return {
            labels: hours.map(h => h.format('HH:mm')),
            datasets: [{
                data,
                label: 'Клики',
                backgroundColor: '#007bff'
            }],
        };
    }

    render() {
        return (
            <div>
                <Bar data={this.data}/>
            </div>
        )
    }
}
