let path = require("path"),
    fs = require("fs"),
    webpack = require("webpack"),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    entry = require('./entry'),
    proxyConfig = require('./wds-proxy'),
    src = path.resolve(__dirname, "../src"),
    mode = process.env.WEBPACK_MODE || 'development',
    dist = path.resolve(__dirname, "../static/admin_frontend");

let proxyConfigLocal = path.resolve(__dirname, 'wds-proxy.local.json');
if (fs.existsSync(proxyConfigLocal)) {
    proxyConfig = Object.assign(proxyConfig, require(proxyConfigLocal))
}

let config = {
    context: src,
    entry,
    output: {
        path: dist,
        filename: 'js/[name].bundle.js?[hash]',
        publicPath: '/static/admin_frontend/'
    },
    target: 'web',
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        // Fix webpack's default behavior to not load packages with jsnext:main module
        // (jsnext:main directs not usually distributable es6 format, but es6 sources)
        mainFields: ['module', 'browser', 'main'],
        alias: {
            Assets: path.resolve(src, '../assets')
        }
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {test: /\.html$/, use: 'html-loader'},
            {test: /\.css$/, loaders: ['style-loader', 'css-loader']},
            {
                test: /\.(png|jpe?g|gif)(\?\S*)?$/,
                loader: 'file-loader',
                query: {name: 'images/[name].[ext]?[hash]'}
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
                loader: 'file-loader',
                query: {name: 'fonts/[name].[ext]?[hash]'}
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'assets/index.html',
            filename: '../../templates/index.html'
        })
    ],
    devServer: {
        overlay: true,
        ...proxyConfig
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    chunks: 'initial',
                    name: 'vendor',
                    test: 'vendor',
                    enforce: true
                },
            }
        },
    },
    node: {
        fs: 'empty',
        net: 'empty'
    },
    devtool: "source-map"
};

module.exports = config;