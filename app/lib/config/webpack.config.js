let path = require("path"),
    src = path.resolve(__dirname, "../src"),
    dist = path.resolve(__dirname, "../static/lib");

let config = {
    context: src,
    entry: './index.ts',
    output: {
        path: dist,
        filename: 'js/main.bundle.js?[hash]',
        publicPath: '/static/lib/'
    },
    target: 'web',
    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
        // Fix webpack's default behavior to not load packages with jsnext:main module
        // (jsnext:main directs not usually distributable es6 format, but es6 sources)
        mainFields: ['module', 'browser', 'main']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/
            }
        ],
    },
    node: {
        fs: 'empty',
        net: 'empty'
    },
    devtool: "source-map"
};

module.exports = config;