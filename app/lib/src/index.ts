import * as shm from 'simpleheat';

let API = (window as any).IT_SUM_TEST_API;
if (typeof API === 'undefined' || API === null) {
    API = 'http://it-sum-test.local.irkdev.ru/api/click-public/';
    // API = 'http://localhost:8000/api/click-public/';
}
let map = null;
let canvas: HTMLCanvasElement = null;
const RADIUS = 10;
let clicks = [];

function send(data) {
    let r = new XMLHttpRequest();
    r.open('POST', API, true);
    r.setRequestHeader('Content-Type', 'application/json');
    r.setRequestHeader('Accept', 'application/json');
    r.send(JSON.stringify(data));
}

function calcPoints(clicks: any[]) {
    let docWidth = Math.max(window.innerWidth, document.body.scrollWidth);
    return clicks.map(c => {
        let value = clicks.filter(cl => {
            let x = cl.x * docWidth,
                y = cl.y;
            return Math.sqrt((x - c.x * docWidth) ** 2 + (y - c.y) ** 2) < RADIUS / 3
        }).length;
        return [c.x * docWidth, c.y, value];
    });
}

function getSizes() {
    return {
        width: Math.max(document.body.scrollWidth, window.innerWidth),
        height: Math.max(document.body.scrollHeight, window.innerHeight)
    }
}

function renderData(clicks: any[]) {
    // if (map) canvas.remove();
    if (!canvas) canvas = document.createElement('canvas');
    canvas.style.position = 'absolute';
    canvas.style.top = '0';
    canvas.style.left = '0';
    canvas.width = getSizes().width;
    canvas.height = getSizes().height;
    canvas.style.zIndex = '10000'; //big number
    canvas.style.backgroundColor = 'rgba(0, 0, 0, .5)';
    document.body.appendChild(canvas);

    map = shm(canvas);

    let points = calcPoints(clicks);

    map.data(points);
    map.max(Math.max(...points.map(p => p[2])));
    map.radius(RADIUS);
    map.draw();
}

document.addEventListener('click', e => {
    if (map) return;
    let data = {
        x: e.pageX / Math.max(document.body.scrollWidth, window.innerWidth),
        y: e.pageY,
        user_tz: (new Date()).getTimezoneOffset(),
        path: window.location.pathname + window.location.search,
        domain: window.location.host
    };
    send(data)
});

function handleMessage(e) {
    let data = null;
    try {
        data = JSON.parse(e.data)
    } catch (e) {
        return;
    }
    if (!data || !data.message) return;
    let message = data.message;
    if (message == 'clicks') {
        if (!data.clicks) return;
        clicks = data.clicks;
        setTimeout(() => renderData(data.clicks), 100)
    } else if (message == 'overlay') {
        if (data.status === undefined || !canvas) return;
        canvas.style.display = data.status ? 'block' : 'none';
    }
}

window.addEventListener('load', () => {
    window.addEventListener('message', handleMessage);

    window.addEventListener('resize', () => {
        if (!canvas || !map) return;
        canvas.height = getSizes().height;
        canvas.width = getSizes().width;
        map.resize();
        map.data(calcPoints(clicks));
        map.draw();
    })
});