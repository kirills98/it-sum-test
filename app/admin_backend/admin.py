from django.contrib import admin

# Register your models here.
from admin_backend.models.click import Click
from admin_backend.models.site import Site

admin.site.register([
    Site,
    Click
])
