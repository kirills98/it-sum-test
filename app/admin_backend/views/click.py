from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, generics

from admin_backend.models.click import Click
from admin_backend.serializers.click import ClickSerializer, ClickPublicSerializer, ClickSearchSerializer


class ClickSearchViewSet(generics.ListAPIView):
    serializer_class = ClickSearchSerializer
    queryset = Click.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {'path': ['contains'], 'site': ['exact']}

    def get_queryset(self):
        return self.queryset.filter(site__user=self.request.user).order_by('path').values("path").distinct()


class ClickViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Click.objects.all()
    serializer_class = ClickSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {'site': ['exact'], 'created_at': ['lte', 'gte'], 'path': ['exact']}

    def get_queryset(self):
        return self.queryset.filter(site__user=self.request.user).order_by('created_at')


class ClickPublicView(generics.CreateAPIView):
    permission_classes = []
    queryset = Click.objects.all()
    serializer_class = ClickPublicSerializer
