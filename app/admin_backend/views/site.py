from django.db.models import Count
from rest_framework import viewsets

from admin_backend.models.click import Click
from admin_backend.models.site import Site
from admin_backend.serializers.site import SiteSerializer


class SiteViewSet(viewsets.ModelViewSet):
    queryset = Site.objects.all()
    serializer_class = SiteSerializer

    def perform_destroy(self, instance: Site):
        Click.objects.annotate(num_sites=Count('site')).filter(num_sites__lte=1, site=instance).delete()
        instance.delete()

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
