from django.db import models


class Click(models.Model):
    path = models.CharField(max_length=1000)
    x = models.FloatField(default=0)
    y = models.FloatField(default=0)
    user_tz = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Клик'
        verbose_name_plural = 'Клики'
