from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from admin_backend.models.click import Click


class Site(models.Model):
    name = models.CharField(max_length=50)
    domains = ArrayField(models.CharField(max_length=200))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    clicks = models.ManyToManyField(Click)

    @staticmethod
    def parse_domain(domain):
        import re
        p = re.compile('^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^\/?\n]+)', re.IGNORECASE + re.MULTILINE)
        return p.match(domain).group(1)

    class Meta:
        verbose_name = 'Сайт'
        verbose_name_plural = 'Сайты'
