from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from admin_backend.views.click import ClickViewSet, ClickPublicView, ClickSearchViewSet
from admin_backend.views.site import SiteViewSet

router = routers.DefaultRouter()
router.register(r'site', SiteViewSet)
router.register(r'click', ClickViewSet)

urlpatterns = [
    url(r'^click-public/$', ClickPublicView.as_view()),
    url(r'^click/search/$', ClickSearchViewSet.as_view())
]
urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns.extend([
    url(r'^', include(router.urls)),
])
