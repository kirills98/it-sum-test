from rest_framework import serializers

from admin_backend.models.click import Click
from admin_backend.models.site import Site


class ClickSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Click
        fields = ('path',)


class ClickSerializer(serializers.ModelSerializer):
    class Meta:
        model = Click
        fields = ('x', 'y', 'path', 'user_tz', 'created_at')


class ClickPublicSerializer(serializers.ModelSerializer):
    domain = serializers.CharField(max_length=200, write_only=True)

    def validate_domain(self, value):
        domain = Site.parse_domain(value)
        sites = Site.objects.filter(domains__contains=[domain])
        if sites.count() <= 0:
            raise serializers.ValidationError('Not found sites by domain')
        return domain

    def create(self, validated_data):
        domain = validated_data.pop('domain')
        sites = Site.objects.filter(domains__contains=[domain])
        click = Click.objects.create(**validated_data)
        click.site_set.add(*sites)
        return click

    class Meta:
        model = Click
        fields = ('x', 'y', 'path', 'user_tz', 'domain')
