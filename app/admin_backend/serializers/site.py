from rest_framework import serializers

from admin_backend.models.site import Site


class SiteSerializer(serializers.ModelSerializer):
    domains = serializers.ListField(child=serializers.CharField(max_length=200))

    def validate_domains(self, value):
        result = []
        for d in value:
            result.append(Site.parse_domain(d))
        return result

    class Meta:
        model = Site
        fields = ('name', 'domains', 'pk')
