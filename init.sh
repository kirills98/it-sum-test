#!/bin/bash
#npm run --prefix /code/app prod
#npm run --prefix /code/app lib-prod

python /code/app/manage.py collectstatic --noinput -c
envsubst < /code/uwsgi/uwsgi.ini.template > /code/uwsgi/uwsgi.ini
uwsgi --ini /code/uwsgi/uwsgi.ini