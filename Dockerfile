FROM python:3
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN mkdir /code
WORKDIR /code

RUN pip install uwsgi
RUN curl --silent --location https://deb.nodesource.com/setup_8.x | bash -

RUN apt update \
    && apt install -y --no-install-recommends \
    gettext-base \
    nodejs \
    build-essential \
    && apt-get -y autoclean \
    && rm -rf /var/lib/apt/lists/*

ADD ./app/requirements.txt /code/app/
RUN pip install -r ./app/requirements.txt

ADD ./app/package.json /code/app
WORKDIR /code/app
RUN npm install
WORKDIR /code

ADD ./app /code/app
WORKDIR /code/app
RUN npm run prod
RUN npm run lib-prod
WORKDIR /code

ADD ./init.sh /code/
RUN chmod +x /code/init.sh

CMD sh /code/init.sh